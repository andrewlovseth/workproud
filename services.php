<?php

/*

	Template Name: Services

*/

get_header(); ?>

	<?php get_template_part('template-parts/global/hero'); ?>

	<?php // get_template_part('template-parts/services/page-header'); ?>

	<?php get_template_part('template-parts/services/process'); ?>

	<?php get_template_part('template-parts/services/features'); ?>

	<?php get_template_part('template-parts/footer/form'); ?>

<?php get_footer(); ?>