<?php

/*

	Template Name: Platform

*/

get_header(); ?>

	<?php get_template_part('template-parts/global/hero'); ?>

	<?php // get_template_part('template-parts/platform/page-header'); ?>

	<?php get_template_part('template-parts/platform/features'); ?>

	<?php get_template_part('template-parts/footer/form'); ?>

<?php get_footer(); ?>