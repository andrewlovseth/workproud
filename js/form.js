(function ($, window, document, undefined) {

    function initContactForm() {

        //delay injecting OID on to contact form for 5 seconds
        //to prevent spam bots from submitting junk
        setTimeout(function() {

            var id = "00D700000008OEb";
            $('input[name="oid"]').val(id);

        }, 5000);


        //validate the form on blur
        $('#ContactForm').on('blur', 'input[required]', validator.checkField);
        $('#ContactForm').on('blur', 'textarea[required]', validator.checkField);


        //validate on submit
        $('#ContactForm').submit(function (e) {

            e.preventDefault();

            var isValid = true;

            if (!validator.checkAll($(this))) {
                isValid = false;
            }

            if (isValid) {
                //does nothing right now
                //we should do this with ajax
                //and change the contents of the
                //form to a 'thanks' message

                this.submit();
            }

            return false;
        });
    }


    /* referrer for tour request */
    function set_referrer() {
        // set referrer cookie if the referrer is not from online-rewards.com
        // var referrer_re = new RegExp("http://(www\.)?online-rewards\.com", "i");
        if (document.referrer && document.referrer != "" && document.cookie.indexOf("referrer=") == -1)
        {
            var current_referrer = document.referrer;

            // remove googl noise
            current_referrer = current_referrer.replace(/&(?:ai|ei|sig|ved)=[\w\-]*/g, "");

            var expDays = 90;
            var exp = new Date();
            exp.setTime(exp.getTime() + (expDays * 24 * 60 * 60 * 1000));
            document.cookie = "referrer=" + escape(current_referrer) + "; expires=" + exp.toGMTString() + "; path=/";
        }
    }


    function get_referrer() {
        var referrer = "";
        if (document.cookie.length > 0)
        {
            var offset = document.cookie.indexOf("referrer=");
            // if referrer cookie exists
            if (offset != -1)
            {
                // 9 is the length of the "referrer=" string
                offset += 9;
                // set index of beginning of value
                var end = document.cookie.indexOf(";", offset);
                // set index of end of cookie value
                if (end == -1) end = document.cookie.length;
                // get the referrer from teh cookie
                referrer = unescape(document.cookie.substring(offset, end));
            }
        }
        return referrer;
    }


    function setFormReferrers() {
        var ref = get_referrer();
        if (ref) $('.referrer').val(ref);
    }


    function setReturnUrls() {
        //get current domain root
        var full = location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
        $('#TourRetURL').val(full + '/demo/thanks/');
    }


    function getQS(name, url) {
        //thank you http://stackoverflow.com/questions/901115/how-can-i-get-query-string-values-in-javascript
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }


    function setSalesForceReferrers() {
        var vals = [
            { name: 'type',       id: '00N70000003rRht'},
            { name: 'campaign',   id: '00N70000003rRho'},
            { name: 'adgroupid',  id: '00N70000003rRhj'},
            { name: 'keyword',    id: '00N70000003rRhe'},
            { name: 'matchtype',  id: '00N70000003rRhZ'},
            { name: 'device',     id: '00N70000003rRhU'},
            { name: 'gclid',      id: '00N70000003rRhP'}
        ];

        for (var i = vals.length - 1; i >= 0; i--) {

            v = getQS(vals[i].name);

            if (v && v != "") Cookies.set(vals[i].name, v, {expires: 365, sameSite: 'lax'});

            //add cookie valus to forms that post offsite

            $('form').each(function() {

                var frm = $(this);

                if (frm.attr('action').startsWith('http')) {

                    if (Cookies.get(vals[i].name) && Cookies.get(vals[i].name) != "") {
                        var input = $('<input/>', {
                            id: vals[i].id,
                            name: vals[i].id,
                            "data-source-name": vals[i].name,
                            type:"hidden",
                            value: Cookies.get(vals[i].name)
                        });
                        input.appendTo(frm);
                    }
                }
            });
        }
    }

    $(function () {

        set_referrer();
        setFormReferrers();
        setReturnUrls();
        setSalesForceReferrers();

    });

})(jQuery, window, document);