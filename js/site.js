(function ($, window, document, undefined) {

	$(document).ready(function($) {

		// rel="external"
		$('a[rel="external"]').click( function() {
			window.open( $(this).attr('href') );
			return false;
		});
		

		// Nav Trigger
		$('.nav-trigger').click(function(){
			$('body').toggleClass('nav-overlay-open');
			return false;
		});


		// Testimonials Slideshow
		$('.testimonials-slider').slick({
			dots: true,
			arrows: false,
			infinite: true,
			speed: 500,
			//adaptiveHeight: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			mobileFirst: true,
			autoplay: true,
			autoplaySpeed: 8000,
		});


		$('html').click(function() {
			$('body').removeClass('nav-overlay-open');
		});

		$('.site-nav.mobile').click(function(event){
		    event.stopPropagation();
		});


	});


	// Escape
	$(document).keyup(function(e) {
		
		if (e.keyCode == 27) {
			$('body').removeClass('nav-overlay-open');
		}

	});


})(jQuery, window, document);