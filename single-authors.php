<?php

$posts_page_id = get_option('page_for_posts');

get_header(); ?>

	<section class="page-header">
		<div class="wrapper">
			<div class="headline">
				<h4><a href="<?php echo get_permalink($posts_page_id ); ?>">← Back to <?php echo get_the_title($posts_page_id); ?></a></h4>
				<h1 class="editorial small">
					<img src="<?php $image = get_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					<span class="name">
						Articles by <?php echo get_the_title(); ?>
					</span>
				</h1>
			</div>
		</div>
	</section>

	<section class="latest-posts">
		<div class="wrapper">

			<?php $author = $post->ID; ?>

			<div class="posts-wrapper">
				<?php echo do_shortcode('[ajax_load_more container_type="div" post_type="post" posts_per_page="9" meta_key="author" meta_value="' . $author . '" meta_compare="LIKE" scroll="false" images_loaded="true" button_label="Load More Posts" button_loading_label="Loading"]'); ?>
			</div>

		</div>
	</section>

	
<?php get_footer(); ?>