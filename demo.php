<?php

/*

	Template Name: Demo

*/

get_header(); ?>

	<?php get_template_part('template-parts/global/hero'); ?>
	
	<section class="form">
		<div class="wrapper">
			
			<div class="headline">
				<?php if(get_field('custom_form_headline')): ?>
					<h3><?php the_field('custom_form_headline'); ?></h3>
				<?php else: ?>
					<h3><?php the_field('form_headline', 'options'); ?></h3>
				<?php endif; ?>
			</div>

			<div class="form-wrapper">
				<?php if(get_field('custom_form')): ?>
					<?php the_field('custom_form'); ?>
				<?php else: ?>
					<?php the_field('form_code', 'options'); ?>
				<?php endif; ?>
			</div>

		</div>
	</section>

<?php get_footer(); ?>