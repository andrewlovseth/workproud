<?php

/*

	Template Name: About

*/

get_header(); ?>

	<?php get_template_part('template-parts/global/hero'); ?>

	<?php get_template_part('template-parts/about/company-performance'); ?>

	<?php get_template_part('template-parts/about/info'); ?>

	<?php get_template_part('template-parts/footer/form'); ?>

<?php get_footer(); ?>