<?php

/*

	Template Name: App

*/

get_header(); ?>

	<?php get_template_part('template-parts/global/hero'); ?>

	<section class="download">
		<div class="wrapper">
			
			<div class="headline">
				<h2><?php the_field('download_headline'); ?></h2>
			</div>

			<div class="links">
				<?php if(have_rows('download_links')): while(have_rows('download_links')): the_row(); ?>
				 
				    <div class="link">
				        <a href="<?php the_sub_field('link'); ?>" rel="external">
				        	<img src="<?php $image = get_sub_field('graphic'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				        </a>
				    </div>

				<?php endwhile; endif; ?>

			</div>

		</div>
	</section>

	<?php get_template_part('template-parts/footer/form'); ?>

<?php get_footer(); ?>