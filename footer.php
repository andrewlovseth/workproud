	</main> <!-- .site-content -->

	<footer class="site-footer">
		<div class="wrapper">
			
            <?php get_template_part('template-parts/footer/logo'); ?>

            <?php get_template_part('template-parts/footer/nav'); ?>

            <?php get_template_part('template-parts/footer/platform'); ?>

            <?php get_template_part('template-parts/footer/legal'); ?>

		</div>
	</footer>

</div> <!-- .site -->

<?php wp_footer(); ?>

<?php the_field('body_bottom', 'options'); ?>

</body>
</html>