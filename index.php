<?php

$posts_page_id = get_option('page_for_posts');

get_header(); ?>

	<section class="page-header">
		<div class="wrapper">
			<div class="headline">
				<h1 class="editorial"><?php echo get_the_title($posts_page_id); ?></h1>
			</div>

			<div class="utilities">
				<div class="filters">
					<?php $terms = get_terms( 'category', 'orderby=count&hide_empty=1'); if($terms): ?>

						<div class="links">
							<div class="link link-header">
								<h5>Filter By:</h5>
							</div>
							<?php foreach ( $terms as $term ): ?>
								<div class="link">
									<a href="<?php echo get_term_link($term); ?>"><?php echo $term->name; ?></a>
								</div>
							<?php endforeach; ?>			
						</div>
		
					<?php endif; ?>
				</div>

				<div class="search-wrapper">
					<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
				        <input type="search" class="search-field"
				            placeholder="<?php echo esc_attr_x( 'Search', 'placeholder' ) ?>"
				            value="<?php echo get_search_query() ?>" name="s"
				            title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
					    <input type="submit" class="search-submit"
					        value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
					</form>
				</div>
				
			</div>

		</div>
	</section>

	<section class="editorial">
		<div class="wrapper">

			<?php $featured_post = get_field('featured_post', $posts_page_id); if( $featured_post ): ?>

				<?php include( locate_template( 'template-parts/blog/featured-post.php', false, false ) ); ?>

			<?php endif; ?>

			<?php $curated_posts = get_field('curated_posts', $posts_page_id); if( $curated_posts ): ?>
				
				<?php include( locate_template( 'template-parts/blog/curated-posts.php', false, false ) ); ?>

			<?php endif; ?>

		</div>
	</section>


	<section class="latest-posts">
		<div class="wrapper">

			<div class="headline section-header">
				<h4>Latest Posts</h4>
			</div>
			
			<div class="posts-wrapper">
				<?php echo do_shortcode('[ajax_load_more container_type="div" post_type="post" posts_per_page="9" scroll="false" images_loaded="true" button_label="Load More Posts" button_loading_label="Loading"]'); ?>
			</div>

		</div>
	</section>

	
<?php get_footer(); ?>