<?php

$posts_page_id = get_option('page_for_posts');

get_header(); ?>

	<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

		<section class="article-wrapper">
			<div class="wrapper">
				
				<article <?php post_class(); ?>>
					<section class="article-header">
						<div class="headline">
							<h4><a href="<?php echo get_permalink($posts_page_id ); ?>">← Back to <?php echo get_the_title($posts_page_id); ?></a></h4>
							<h1 class="editorial small"><?php the_title(); ?></h1>
						</div>

						<?php get_template_part('template-parts/single/share'); ?>

						<?php if(get_field('featured_image')): ?>
							<div class="featured-image">
								<div class="square">
									<img src="<?php $image = get_field('featured_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />									
								</div>								
							</div>
						<?php endif; ?>
							
					</section>

					<section class="article-body">
						<div class="copy p3 extended">
							<?php the_content(); ?>
						</div>					
					</section>
				</article>

				<aside>
					<?php get_template_part('template-parts/single-post/share'); ?>

					<?php get_template_part('template-parts/single-post/date'); ?>

					<?php get_template_part('template-parts/single-post/authors'); ?>

					<?php get_template_part('template-parts/single-post/newsletter'); ?>

					<?php get_template_part('template-parts/single-post/categories'); ?>

					<?php get_template_part('template-parts/single-post/search'); ?>

					<?php get_template_part('template-parts/single-post/recent-posts'); ?>
				</aside>

			</div>		
		</section>

	<?php endwhile; endif; ?>

	<?php get_template_part('template-parts/layout/tour-form'); ?>
	
<?php get_footer(); ?>