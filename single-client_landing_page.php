<?php get_header(); ?>

	<?php if (post_password_required()): ?>

		<?php get_template_part('template-parts/client-landing-page/gateway'); ?>

	<?php else: ?>

		<?php get_template_part('template-parts/client-landing-page/hero'); ?>

		<?php get_template_part('template-parts/client-landing-page/video'); ?>

		<?php get_template_part('template-parts/client-landing-page/about'); ?>

		<?php get_template_part('template-parts/client-landing-page/technology'); ?>

		<?php get_template_part('template-parts/client-landing-page/clients'); ?>

	<?php endif; ?>
	
<?php get_footer(); ?>