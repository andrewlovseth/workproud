<?php

/*

	Template Name: Why

*/

get_header(); ?>

	<?php get_template_part('template-parts/global/hero'); ?>

	<?php get_template_part('template-parts/why/intro'); ?>

	<?php get_template_part('template-parts/why/how'); ?>

	<?php get_template_part('template-parts/footer/form'); ?>

<?php get_footer(); ?>