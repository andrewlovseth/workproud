<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	
	<?php wp_head(); ?>

	<?php the_field('head', 'options'); ?>

</head>

<body <?php body_class(); ?>>

<?php wp_body_open(); ?>

<?php the_field('body_top', 'options'); ?>

<div id="page" class="site">
	
	<header class="site-header">
		<div class="wrapper">
			
			<?php get_template_part('template-parts/header/site-logo'); ?>

			<?php get_template_part('template-parts/header/site-nav-desktop'); ?>

			<?php get_template_part('template-parts/header/demo-btn'); ?>

			<?php get_template_part('template-parts/header/hamburger'); ?>

		</div>
	</header>

	<?php get_template_part('template-parts/header/site-nav-mobile'); ?>

	<main class="site-content">	
