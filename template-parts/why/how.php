<div class="diagonal top">
	<div class="shape rtl">
	</div>
</div>

<section class="how">
	<div class="wrapper">
		
		<div class="headline section-header">
			<h3><?php the_field('how_headline'); ?></h3>
		</div>

		<div class="features">
			<?php if(have_rows('how_features')): while(have_rows('how_features')): the_row(); ?>

				<div class="feature">										 
					<div class="photo">
						<div class="content">
							<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						</div>						
					</div>

					<div class="info">
						<div class="headline">
							<h4><?php the_sub_field('headline'); ?></h4>
						</div>

						<div class="copy p3">
							<?php the_sub_field('deck'); ?>
						</div>

						<?php 
							$link = get_sub_field('cta');
							if( $link ): 
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
						 ?>

						 	<div class="cta">
						 		<a class="underline-btn" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
						 	</div>

						<?php endif; ?>
					</div>
				</div>

			<?php endwhile; endif; ?>
							
		</div>

	</div>
</section>

<div class="diagonal bottom">
	<div class="shape rtl">
	</div>
</div>