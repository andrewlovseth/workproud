<section class="intro">
	<div class="wrapper">
			
		<div class="graphic">
			<img src="<?php $image = get_field('intro_graphic'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		</div>

		<div class="info">
			<div class="headline">
				<h3><?php the_field('intro_headline'); ?></h3>
			</div>

			<div class="copy p2">
				<?php the_field('intro_deck'); ?>
			</div>
		</div>

	</div>
</section>