<section class="hero">
	<div class="content">

		<img class="hero-image" src="<?php $image = get_field('hero_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

		<div class="hero-content">
			<div class="wrapper">
				<div class="info">
					
					<?php if(get_field('hero_sub_headline')): ?>
						<div class="sub-headline">
							<h4><?php the_field('hero_sub_headline'); ?></h4>
						</div>
					<?php endif; ?>

					<div class="headline">
						<h1><?php the_field('hero_headline'); ?></h1>
					</div>

					<div class="copy p1">
						<?php the_field('hero_copy'); ?>
					</div>
					
				</div>
			</div>			
		</div>

	</div>
</section>

<div class="diagonal bottom">
	<div class="shape ltr bottom">
	</div>
</div>