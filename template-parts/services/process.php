<div class="diagonal top">
	<div class="shape rtl">
	</div>
</div>

<section class="process">
	<div class="wrapper">

		<div class="section-header headline">
			<h3><?php the_field('process_headline'); ?></h3>
		</div>

		<div class="grid three-col process-gallery">
			<?php if(have_rows('process')): while(have_rows('process')): the_row(); ?>
			 
			    <div class="grid-item">
			    	<div class="icon">
			    		<img src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			    	</div>

			    	<div class="headline">
			    		<h5><?php the_sub_field('headline'); ?></h5>
			    	</div>

			    	<div class="copy p4">
			    		<?php the_sub_field('deck'); ?>
			    	</div>			        
			    </div>

			<?php endwhile; endif; ?>			
		</div>

	</div>
</section>

<div class="diagonal bottom">
	<div class="shape rtl">
	</div>
</div>