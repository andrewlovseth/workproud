<section class="page-header">
	<div class="wrapper">

		<div class="headline">
			<h2><?php the_field('page_header_headline'); ?></h2>
		</div>

		<div class="copy p2">
			<?php the_field('page_header_deck'); ?>
		</div>

	</div>
</section>