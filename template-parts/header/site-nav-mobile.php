<nav class="site-nav mobile">
	<div class="nav-wrapper">

		<div class="link home">
			<a href="<?php echo site_url('/'); ?>">Home</a>
		</div>


		<?php if(have_rows('header_navigation', 'options')): while(have_rows('header_navigation', 'options')): the_row(); ?>
		 
		    <div class="link <?php the_sub_field('link'); ?>">
		    	<a href="/<?php the_sub_field('link'); ?>/">
		    		<?php the_sub_field('label'); ?>
		    	</a>				        
		    </div>

		<?php endwhile; endif; ?>		

		<?php 
			$link = get_field('demo_link', 'options');
			if( $link ): 
			$link_url = $link['url'];
			$link_title = $link['title'];
			$link_target = $link['target'] ? $link['target'] : '_self';
		 ?>

		 	<div class="link demo">
		 		<a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
		 	</div>

		<?php endif; ?>

	</div>
</nav>