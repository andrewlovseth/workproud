<nav class="site-nav desktop">
	<?php if(have_rows('header_navigation', 'options')): while(have_rows('header_navigation', 'options')): the_row(); ?>
	 
	    <div class="link <?php the_sub_field('link'); ?>">
	    	<a href="/<?php the_sub_field('link'); ?>/">
	    		<?php the_sub_field('label'); ?>
	    	</a>				        
	    </div>

	<?php endwhile; endif; ?>				
</nav>