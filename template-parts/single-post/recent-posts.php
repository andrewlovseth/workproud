<div class="recent-posts">
	<h5>Recent Posts</h5>

	<div class="posts-wrapper">
		<?php
			$current_post = $post->ID;
			$args = array(
				'post_type' => 'post',
				'posts_per_page' => 5,
				'post__not_in' => array($current_post)
			);
			$query = new WP_Query( $args );
			if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

			<article class="recent">
				<div class="info">
					<div class="headline">
						<h4><?php the_time('n/j/Y'); ?></h4>
					</div>

					<div class="copy p4">
						<p>
							<a href="<?php the_permalink(); ?>">
								<?php the_title(); ?>
							</a>
						</p>						
					</div>
				</div>
			</article>

		<?php endwhile; endif; wp_reset_postdata(); ?>
	</div>
</div>