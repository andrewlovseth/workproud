<div class="newsletter">
	<h5>Newsletter</h5>

	<div class="copy p3 small">
		<p>Subscribe to receive news & updates</p>
	</div>

	<?php 
		$posts_page_id = get_option('page_for_posts');
		the_field('newsletter_form', $posts_page_id);
	?>
</div>