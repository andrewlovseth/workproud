<?php $categories = get_the_category(); if(!empty($categories)): ?>
	<div class="category">
		<h5>Category</h5>

		<div class="copy p4 small">
			<p>
				<a href="<?php echo esc_url( get_category_link( $categories[0]->term_id ) ); ?>">
					<?php echo esc_html( $categories[0]->name ); ?>
				</a>
			</p>
		</div>
	</div>
<?php endif; ?>