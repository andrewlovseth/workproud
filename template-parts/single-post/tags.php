<?php if(get_the_tags()): ?>
	<div class="tags">
		<h5>Tags</h5>

		<div class="copy p3 small">
			<p>
				<?php the_tags('', ', ', ''); ?>
			</p>
		</div>
	</div>
<?php endif; ?>
