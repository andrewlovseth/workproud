<div class="share">
	<h5>Share</h5>

	<div class="links">
		<div class="link facebook">
			<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink(); ?>">
				<img src="<?php bloginfo('template_directory') ?>/images/share-facebook.svg" alt="Facebook">
			</a>
		</div>

		<div class="link twitter">
			<a href="https://twitter.com/intent/tweet/?text=<?php echo get_the_title(); ?>+<?php echo get_permalink(); ?>">
				<img src="<?php bloginfo('template_directory') ?>/images/share-twitter.svg" alt="Twitter">
			</a>
		</div>

		<div class="link linkedin">
			<a href="https://www.linkedin.com/shareArticle/?mini=true&url=<?php echo get_permalink(); ?>&title=<?php echo get_the_title(); ?>">
				<img src="<?php bloginfo('template_directory') ?>/images/share-linkedin.svg" alt="LinkedIn">
			</a>
		</div>
	</div>

</div>