<?php $authors = get_field('author'); if( $authors ): ?>
	<div class="authors">

		<h5>Author<?php if(count($authors) > 1): ?>s<?php endif; ?></h5>

	    <?php foreach( $authors as $author): ?>

	    	<div class="author">
	    		<div class="photo">
	    			<a href="<?php echo get_permalink($author->ID); ?>">
	    				<img src="<?php $image = get_field('photo', $author->ID); echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
	    			</a>
	    		</div>
	    		<div class="copy p4 small">
			    	<p>
			    		<a href="<?php echo get_permalink($author->ID); ?>"><?php echo get_the_title($author->ID); ?></a>
			        </p>
	    		</div>
	    	</div>

	    <?php endforeach; ?>

	</div>
<?php endif; ?>