<div class="diagonal top">
	<div class="shape rtl">
	</div>
</div>

<section class="companies">
	<div class="wrapper">
	
		<div class="headline">
			<h3><?php the_field('companies_headline'); ?></h3>
		</div>

		<div class="gallery grid four-col">
			<?php $images = get_field('companies_gallery'); if( $images ): ?>
				<?php foreach( $images as $image ): ?>

					<div class="logo">
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>

				<?php endforeach; ?>
			<?php endif; ?>
		</div>

	</div>
</section>


<div class="diagonal bottom">
	<div class="shape rtl">
	</div>
</div>