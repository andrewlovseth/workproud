<section class="why">
	<div class="wrapper">

		<div class="info">
			<div class="headline">
				<h3><?php the_field('why_headline'); ?></h3>
			</div>

			<div class="copy p2">
				<?php the_field('why_deck'); ?>
			</div>
			
			<?php 
				$link = get_field('why_cta');
				if( $link ): 
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self';
			 ?>

			 	<div class="cta">
			 		<a class="small-red-btn" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
			 	</div>

			<?php endif; ?>
		</div>


		<div class="graphic">
			<img src="<?php $image = get_field('why_graphic'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			
		</div>


	</div>
</section>