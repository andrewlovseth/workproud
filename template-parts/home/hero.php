<section class="home-hero">
	<div class="content">		

		<div class="wrapper">
			<div class="info">
				<div class="headline">
					<h1><?php the_field('hero_headline'); ?></h1>
				</div>

				<div class="copy p1">
					<?php the_field('hero_deck'); ?>
				</div>

				<?php 
					$link = get_field('hero_cta');
					if( $link ): 
					$link_url = $link['url'];
					$link_title = $link['title'];
					$link_target = $link['target'] ? $link['target'] : '_self';
				 ?>
				 
				 	<div class="cta">
				 		<a class="small-red-btn" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
				 	</div>

				<?php endif; ?>				
			</div>

			<div class="graphic">
				<img src="<?php $image = get_field('hero_graphic'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				
			</div>
		</div>

	</div>
</section>

<div class="diagonal bottom">
	<div class="shape ltr bottom">
	</div>
</div>