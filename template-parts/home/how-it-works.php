<section class="how-it-works">
	<div class="wrapper">

		<?php 
			$link = get_field('how_it_works_link');
			$link_url = $link['url'];
			$link_title = $link['title'];
			$link_target = $link['target'] ? $link['target'] : '_self';
		 ?>

		<div class="thumbnail">
			<a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
				<img src="<?php $image = get_field('how_it_works_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</a>
		</div>

		<div class="info">
			<div class="headline">
				<h3><?php the_field('how_it_works_headline'); ?></h3>
			</div>

			<div class="copy p2">
				<?php the_field('how_it_works_deck'); ?>
			</div>
			
			<?php if( $link ): ?>
			 	<div class="cta">
			 		<a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>" class="small-red-btn wide">
			 			<?php echo esc_html($link_title); ?>
			 		</a>
			 	</div>
			<?php endif; ?>
		</div>

	</div>
</section>