<section class="latest-posts">
	<div class="wrapper">

		<div class="headline section-header">
			<h3><?php the_field('insights_headline'); ?></h3>
		</div>
		
		<div class="posts-wrapper">
			<?php
				$args = array(
					'post_type' => 'post',
					'posts_per_page' => 3
				);
				$query = new WP_Query( $args );
				if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>


					<?php get_template_part('template-parts/blog/post'); ?>

			<?php endwhile; endif; wp_reset_postdata(); ?>
		</div>

		<?php 
			$link = get_field('insights_cta');
			if( $link ): 
			$link_url = $link['url'];
			$link_title = $link['title'];
			$link_target = $link['target'] ? $link['target'] : '_self';
		 ?>

		 	<div class="cta section-cta">
		 		<a class="small-red-btn" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
		 	</div>

		<?php endif; ?>

	</div>
</section>