<section class="testimonials">
	<div class="wrapper">

		<div class="testimonials-slider">
			<?php if(have_rows('testimonials')): while(have_rows('testimonials')): the_row(); ?>
			 
			    <div class="quote">
			    	<div class="pullquote">
			    		<p><?php the_sub_field('quote'); ?></p>
			    	</div>

			    	<div class="logo">
			    		<img src="<?php $image = get_sub_field('logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			    	</div>
			       
			    </div>

			<?php endwhile; endif; ?>
		</div>

	</div>
</section>