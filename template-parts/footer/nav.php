<nav class="footer-nav grid four-col">
    <?php if(have_rows('footer_nav', 'options')): while(have_rows('footer_nav', 'options')) : the_row(); ?>

        <?php if( get_row_layout() == 'column' ): ?>

            <div class="grid-item column">
                <?php if(have_rows('links')): while(have_rows('links')): the_row(); ?>
                    
                    <div class="link">
                        <a href="<?php the_sub_field('link'); ?>">
                            <?php the_sub_field('label'); ?>
                        </a>
                    </div>

                <?php endwhile; endif; ?>						
            </div>

        <?php endif; ?>

    <?php endwhile; endif; ?>


    <?php if(have_rows('contact_info', 'options')): while(have_rows('contact_info', 'options')): the_row(); ?>

        <div class="grid-item column contact-info">

            <div class="contact">
                <div class="headline">
                    <h5><?php the_sub_field('location'); ?></h5>
                </div>

                <div class="copy p4">
                    <p><?php the_sub_field('address'); ?></p>

                    <p><?php the_sub_field('phone'); ?></p>					        	
                </div>
            </div>

        </div>

    <?php endwhile; endif; ?>


</nav>