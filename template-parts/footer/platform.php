<div class="platform">
    <div class="headline">
        <h5><?php the_field('platform_headline', 'options'); ?></h5>
    </div>

    <div class="logos">
        <?php if(have_rows('platform_logos', 'options')): while(have_rows('platform_logos', 'options')): the_row(); ?>
            
            <div class="logo">
                <img src="<?php $image = get_sub_field('logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
            </div>
        <?php endwhile; endif; ?>
    </div>
</div>