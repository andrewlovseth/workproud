<div class="legal">
    <div class="copyright">
        <p><?php the_field('copyright', 'options'); ?></p>
    </div>

    <div class="links">
        <?php if(have_rows('footer_legal_links', 'options')): while(have_rows('footer_legal_links', 'options')): the_row(); ?>
            
            <div class="link">
                <a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('label'); ?></a>.
            </div>

        <?php endwhile; endif; ?>	
    </div>

    <div class="social">
        <a href="<?php the_field('twitter', 'options'); ?>" class="twitter" rel="external">
            <img src="<?php bloginfo('template_directory'); ?>/images/twitter.svg" alt="Twitter" />
        </a>
        
        <a href="<?php the_field('linkedin', 'options'); ?>" class="linkedin" rel="external">
            <img src="<?php bloginfo('template_directory'); ?>/images/linkedin.svg" alt="LinkedIn" />
        </a>
        
        <a href="<?php the_field('facebook', 'options'); ?>" class="facebook" rel="external">
            <img src="<?php bloginfo('template_directory'); ?>/images/facebook.svg" alt="Facebook" />
        </a>
        
        <a href="<?php the_field('instagram', 'options'); ?>" class="instagram" rel="external">
            <img src="<?php bloginfo('template_directory'); ?>/images/instagram.svg" alt="Instagram" />
        </a>
    </div>
</div>