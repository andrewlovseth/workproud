<section class="curated-posts" data-aos="fade-left">

	<?php foreach( $curated_posts as $curated_post ): ?>

		<article class="curated-post post">
			<div class="photo">
				<a href="<?php echo get_permalink($curated_post->ID); ?>">
					<img src="<?php $image = get_field('featured_image', $curated_post->ID); echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
				</a>
			</div>

			<div class="info">
				<div class="headline">

					<?php $categories = get_the_category($curated_post->ID);if(!empty($categories)): ?>
						<h4><a href="<?php echo esc_url( get_category_link( $categories[0]->term_id ) ); ?>"><?php echo esc_html( $categories[0]->name ); ?></a></h4>
					<?php endif; ?>

					<h3 class="editorial small"><a href="<?php echo get_permalink($curated_post->ID); ?>"><?php echo get_the_title($curated_post->ID); ?></a></h3>						
				</div>

				<div class="copy p4">
					<p>
						<?php $excerpt = wp_trim_words( get_field('summary', $curated_post->ID), $num_words = 15, $more = '...' ); echo $excerpt; ?>
					</p>
				</div>

			</div>
		</article>

	<?php endforeach; ?>

	<?php if(is_front_page()): ?>
		<div class="cta small">
			<a href="<?php $posts_page_id = get_option('page_for_posts'); echo get_permalink($posts_page_id ); ?>">Explore <?php echo get_the_title($posts_page_id); ?></a>
		</div>
	<?php endif; ?>

</section>