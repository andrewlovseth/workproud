<article class="post">
	<div class="photo">
		<div class="content">
			<a href="<?php the_permalink(); ?>">
				<img src="<?php $image = get_field('featured_image'); echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
			</a>
		</div>
	</div>

	<div class="info">
		<div class="headline">
			<?php $categories = get_the_category(); if(!empty($categories)): ?>
				<h4>
					<a href="<?php echo esc_url( get_category_link( $categories[0]->term_id ) ); ?>">
						<?php echo esc_html( $categories[0]->name ); ?>
					</a>
				</h4>
			<?php endif; ?>

			<h3 class="editorial small"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>						
		</div>

		<div class="copy p4">
			<p>
				<?php $excerpt = wp_trim_words( get_field('summary' ), $num_words = 15, $more = '...' ); echo $excerpt; ?>
			</p>
		</div>

		<div class="cta">
			<a href="<?php the_permalink(); ?>" class="underline-btn">Read More</a>
		</div>
	</div>
</article>
