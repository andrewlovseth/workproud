<article class="featured-post square square-right" data-aos="fade-right">
	<div class="photo">
		<a href="<?php echo get_permalink($featured_post->ID); ?>">
			<img src="<?php $image = get_field('featured_image', $featured_post->ID); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		</a>
	</div>

	<div class="info">
		<div class="headline">
			<?php $categories = get_the_category($featured_post->ID);if(!empty($categories)): ?>
				<h4><a href="<?php echo esc_url( get_category_link( $categories[0]->term_id ) ); ?>"><?php echo esc_html( $categories[0]->name ); ?></a></h4>
			<?php endif; ?>

			<h2 class="editorial"><a href="<?php echo get_permalink($featured_post->ID); ?>"><?php echo get_the_title($featured_post->ID); ?></a></h2>						
		</div>

		<div class="copy p3">
			<?php the_field('summary', $featured_post->ID); ?>
		</div>

		<div class="cta">
			<a class="small-red-btn" href="<?php echo get_permalink($featured_post->ID); ?>">Read more</a>
		</div>
	</div>
</article>
