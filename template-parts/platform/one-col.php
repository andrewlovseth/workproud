<?php if( get_row_layout() == '1_column' ): ?>

	<?php 
		$className = 'one-col-feature platform-section';
		if(get_sub_field('off_white_background') == true) {
		    $className .= ' ' . 'off-white';
		}

		$top_diagonal = get_sub_field('top_diagonal');

		if($top_diagonal['show'] == true) {
		    $className .= ' ' . 'top-diagonal';
		}

		$bottom_diagonal = get_sub_field('bottom_diagonal');

		if($bottom_diagonal['show'] == true) {
		    $className .= ' ' . 'bottom-diagonal';
		}
	?>

	<?php if($top_diagonal['show'] == true): ?>
		<div class="diagonal top">
			<div class="shape <?php echo $top_diagonal['direction']; ?>">
			</div>
		</div>
	<?php endif; ?>

	<section class="<?php echo esc_attr($className); ?>">
		<div class="wrapper">
			
			<div class="flex-content align-<?php the_sub_field('graphic_alignment'); ?>">
				<div class="graphic">
					<img src="<?php $image = get_sub_field('graphic'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>

				<div class="info">
					<div class="headline">
						<h3><?php the_sub_field('headline'); ?></h3>
					</div>

					<div class="copy p2">
						<?php the_sub_field('deck'); ?>
					</div>
				</div>
			</div>

		</div>		
	</section>

	<?php if($bottom_diagonal['show'] == true): ?>
		<div class="diagonal bottom">
			<div class="shape <?php echo $bottom_diagonal['direction']; ?>">
			</div>
		</div>
	<?php endif; ?>

<?php endif; ?>
