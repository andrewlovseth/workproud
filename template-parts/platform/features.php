<?php if(have_rows('platform')): while(have_rows('platform')) : the_row(); ?>

	<?php include(locate_template('template-parts/platform/one-col.php')); ?>

	<?php include(locate_template('template-parts/platform/two-col.php')); ?>

	<?php include(locate_template('template-parts/platform/three-col.php')); ?>

<?php endwhile; endif; ?>