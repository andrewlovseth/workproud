<?php if( get_row_layout() == '3_column' ): ?>

	<?php 
		$className = 'three-col-feature platform-section';
		if(get_sub_field('off_white_background') == true) {
		    $className .= ' ' . 'off-white';
		}

		$top_diagonal = get_sub_field('top_diagonal');

		if($top_diagonal['show'] == true) {
		    $className .= ' ' . 'top-diagonal';
		}

		$bottom_diagonal = get_sub_field('bottom_diagonal');

		if($bottom_diagonal['show'] == true) {
		    $className .= ' ' . 'bottom-diagonal';
		}
	?>

	<?php if($top_diagonal['show'] == true): ?>
		<div class="diagonal top">
			<div class="shape <?php echo $top_diagonal['direction']; ?>">
			</div>
		</div>
	<?php endif; ?>

	<section class="<?php echo esc_attr($className); ?>">
		<div class="wrapper">

			<div class="grid three-col">

				<?php if(have_rows('content')): while(have_rows('content')): the_row(); ?>
	 					 
				    <div class="grid-item">
				    	<div class="icon">
				    		<img src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				    	</div>

				    	<div class="headline">
				    		<h5><?php the_sub_field('headline'); ?></h5>
				    	</div>

				    	<div class="copy p4">
				    		<?php the_sub_field('deck'); ?>
				    	</div>			        
				    </div>

				<?php endwhile; endif; ?>
			
			</div>

		</div>
	</section>

	<?php if($bottom_diagonal['show'] == true): ?>
		<div class="diagonal bottom">
			<div class="shape <?php echo $bottom_diagonal['direction']; ?>">
			</div>
		</div>
	<?php endif; ?>

<?php endif; ?>