<section class="company-performance">
	<div class="wrapper">
		
		<div class="section-header headline">
			<h2><?php the_field('company_performance_headline'); ?></h2>
		</div>

		<div class="grid three-col company-performance-grid">
			<?php if(have_rows('company_performance_grid')): while(have_rows('company_performance_grid')): the_row(); ?>

			    <div class="grid-item">
			    	<div class="content">		    		
				    	<div class="stat">
				    		<h3><?php the_sub_field('stat'); ?></h3>
				    	</div>

				    	<div class="descriptor">
				    		<p><?php the_sub_field('descriptor'); ?></p>
				    	</div>        
			    	</div>
			    </div>

			<?php endwhile; endif; ?>
		</div>

	</div>
</section>