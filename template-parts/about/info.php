<div class="diagonal top">
	<div class="shape rtl">
	</div>
</div>

<section class="info">
	<div class="wrapper">

		<div class="info-wrapper">
			<div class="section-header headline">
				<h3><?php the_field('info_headline'); ?></h3>
			</div>

			<div class="copy p3 extended">
				<?php the_field('info_copy'); ?>
			</div>
		</div>

	</div>
</section>

<div class="diagonal bottom">
	<div class="shape rtl">
	</div>
</div>