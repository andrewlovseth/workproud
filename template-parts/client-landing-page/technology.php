<?php if(get_field('show_technology') == true): ?>

	<section class="technology">
		<div class="wrapper">
			
			<div class="headline">
				<h3><?php the_field('technology_headline'); ?></h3>
			</div>

			<div class="gallery">
				<?php if(have_rows('technology_gallery')): while(have_rows('technology_gallery')): the_row(); ?>
 
				    <div class="item">
				    	<div class="icon">
				    		<img src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				    	</div>
				        
				        <div class="copy p4">
				        	<?php the_sub_field('copy'); ?>
				        </div>
				    </div>

				<?php endwhile; endif; ?>
			</div>

		</div>
	</section>

<?php endif; ?>