<?php if(get_field('show_clients') == true): ?>
	
	<div class="diagonal top clients-top-diagonal">
		<div class="shape ltr">
		</div>
	</div>

	<section class="clients">
		<div class="wrapper">
			
			<div class="headline">
				<h3><?php the_field('clients_headline'); ?></h3>
			</div>

			<div class="gallery">
				<?php $images = get_field('clients_gallery'); if( $images ): ?>
					<?php foreach( $images as $image ): ?>
						<div class="item">
							<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						</div>
					<?php endforeach; ?>
				<?php endif; ?>	
			</div>

		</div>
	</section>

<?php endif; ?>