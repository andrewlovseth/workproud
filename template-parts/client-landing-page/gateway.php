<section class="gateway pre-password">
	<div class="wrapper">

		<div class="client-logo">
			<img src="<?php $image = get_field('client_logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		</div>

		<div class="headline">
			<h3><?php the_field('pre_password_headline'); ?></h3>
		</div>
		
		<div class="copy p4">
			<?php the_field('pre_password_content'); ?>
		</div>

		<div class="password-form">
			<?php echo get_the_password_form(); ?>
		</div>		

	</div>
</section>