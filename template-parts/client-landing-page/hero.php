<section class="hero">
	<div class="content">

		<img class="hero-image" src="<?php $image = get_field('hero_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

		<div class="hero-content">
			<div class="wrapper">
				<div class="info">

					<?php if(get_field('hero_logo_image')): ?>	
						<img src="<?php $image = get_field('hero_logo_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					<?php endif; ?>

					<?php if(get_field('client_name')): ?>
						<div class="sub-headline client-name">
							<h4><?php the_field('client_name'); ?></h4>
						</div>
					<?php endif; ?>

					<div class="headline">
						<h1><?php the_field('hero_headline'); ?></h1>
					</div>
					
					<?php if(get_field('hero_sub_headline')): ?>
						<div class="sub-headline">
							<h3><?php the_field('hero_sub_headline'); ?></h3>
						</div>
					<?php endif; ?>

					<div class="copy p3">
						<?php the_field('hero_copy'); ?>
					</div>

				</div>
			</div>			
		</div>

	</div>
</section>


<div class="diagonal bottom hero-diagonal">
	<div class="shape ltr bottom">
	</div>
</div>