<section class="about-workproud<?php if(get_field('show_technology') == false): ?> no-technology<?php endif; ?>">
	<div class="wrapper">
		
		<div class="about">
			<div class="headline">
				<h3><?php the_field('about_headline'); ?></h3>
				<h4><?php the_field('about_sub_headline'); ?></h4>
			</div>

			<div class="copy p3">
				<?php the_field('about_copy'); ?>
			</div>
		</div>

		<?php if(get_field('show_accolades') == true): ?>

			<div class="accolades">
				<div class="headline">
					<h3><?php the_field('accolades_headline'); ?></h3>
				</div>

				<div class="copy p3">
					<?php the_field('accolades_copy'); ?>
				</div>

				<div class="accolades-gallery">
					<?php if(have_rows('accolades')): while(have_rows('accolades')): the_row(); ?>
					 
					    <div class="accolade">
					    	<div class="content">
						        <div class="icon">
						        	<img src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						        </div>

						        <div class="copy p4">
						        	<p><?php the_sub_field('text'); ?></p>
						        </div>
					    	</div>

					    </div>

					<?php endwhile; endif; ?>
				</div>
			</div>

		<?php endif; ?>

	</div>
</section>

<div class="diagonal bottom about-bottom-diagonal">
	<div class="shape rtl">
	</div>
</div>