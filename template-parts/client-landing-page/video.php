<section class="video">
	<div class="wrapper">

		<div class="copy p2">
			<?php the_field('video_copy_pre'); ?>
		</div>		

		<?php if(have_rows('video')): while(have_rows('video')) : the_row(); ?>

			<?php if( get_row_layout() == 'html_video' ): ?>

				<div class="video-player">
					<?php echo do_shortcode(get_sub_field('shortcode')); ?>			
				</div>

			<?php endif; ?>

		<?php endwhile; endif; ?>

		<div class="copy p2">
			<?php the_field('video_copy'); ?>
		</div>

	</div>
</section>
