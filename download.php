<?php

/*

	Template Name: Download

*/

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	
	<?php wp_head(); ?>

	<?php the_field('head', 'options'); ?>

</head>

<body <?php body_class(); ?>>

<?php wp_body_open(); ?>

<?php the_field('body_top', 'options'); ?>

<div id="page" class="site">

    <header class="download-header">
        <div class="wrapper">

            <?php get_template_part('template-parts/header/site-logo'); ?>
        
        </div>
    </header>

	<main class="site-content">

        <section class="download-app">
            <div class="wrapper">

                <?php if(get_field('headline')): ?>
                    <div class="headline">
                        <h3><?php the_field('headline'); ?></h3>
                    </div>
                <?php endif; ?>

                <?php if(get_field('copy')): ?>
                    <div class="copy p3 extended">
                        <?php the_field('copy'); ?>
                    </div>
                <?php endif; ?>

                <?php if(have_rows('download_links')): ?>
                    <div class="download-links">
                        <?php while(have_rows('download_links')): the_row(); ?>

                            <?php 
                                $link = get_sub_field('link');
                                $isAPK = endsWith($link, ".apk");
                            ?>
                    
                            <div class="link">
                            <a href="<?php echo $link ?>" rel="external"<?php if($isAPK == true): ?> download<?php endif; ?>>
                                    <img src="<?php $image = get_sub_field('graphic'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                                </a>
                            </div>

                        <?php endwhile; ?>                    
                    </div>
                <?php endif; ?>

                <?php if(get_field('additional_copy')): ?>
                    <div class="copy p3 extended additional">
                        <?php the_field('additional_copy'); ?>
                    </div>
                <?php endif; ?>

            </div>
        </section>

	</main> <!-- .site-content -->

    <?php $footer = get_field('show_footer'); if($footer === true): ?>

        <footer class="site-footer">
            <div class="wrapper">
                
                <?php get_template_part('template-parts/footer/logo'); ?>

                <?php get_template_part('template-parts/footer/nav'); ?>

                <?php get_template_part('template-parts/footer/platform'); ?>

                <?php get_template_part('template-parts/footer/legal'); ?>

            </div>
        </footer>

    <?php endif; ?>

</div> <!-- .site -->

<?php wp_footer(); ?>

<?php the_field('body_bottom', 'options'); ?>

</body>
</html>