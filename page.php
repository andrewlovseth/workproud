<?php get_header(); ?>

	<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

		<article class="page-article">
			<div class="wrapper">

				<?php if(get_field('show_logo')): ?>
					<div class="logo">
						<img src="<?php $image = get_field('logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>
				<?php endif; ?>
				
				<div class="headline">
					<h1><?php the_title(); ?></h1>
				</div>

				<div class="copy p3 extended">
					<?php the_content(); ?>
				</div>	

			</div>
		</article>

	<?php endwhile; endif; ?>

<?php get_footer(); ?>