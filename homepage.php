<?php

/*

	Template Name: Home

*/

get_header(); ?>

	<?php get_template_part('template-parts/home/hero'); ?>

	<?php get_template_part('template-parts/home/how-it-works'); ?>

	<?php get_template_part('template-parts/home/why'); ?>

	<?php get_template_part('template-parts/home/companies'); ?>

	<?php get_template_part('template-parts/home/insights'); ?>

	<?php get_template_part('template-parts/footer/form'); ?>

<?php get_footer(); ?>