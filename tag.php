<?php

$posts_page_id = get_option('page_for_posts');

get_header(); ?>

	<section class="page-header">
		<div class="wrapper">
			<div class="headline">
				<h4><a href="<?php echo get_permalink($posts_page_id ); ?>">← Back to <?php echo get_the_title($posts_page_id); ?></a></h4>
				<h1 class="editorial small">Tag: <?php single_tag_title(''); ?></h1>
			</div>
		</div>
	</section>

	<section class="latest-posts">
		<div class="wrapper">

			<?php $tag = get_queried_object(); ?>

			<div class="posts-wrapper">
				<?php echo do_shortcode('[ajax_load_more container_type="div" post_type="post" posts_per_page="9" tag="' . $tag->slug . '" scroll="false" images_loaded="true" button_label="Load More Posts" button_loading_label="Loading"]'); ?>
			</div>

		</div>
	</section>

	
<?php get_footer(); ?>